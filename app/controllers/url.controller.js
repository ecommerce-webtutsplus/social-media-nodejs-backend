const URL = require("../models/url.model.js");

// Create and Save a new URL
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
        message: "Content can not be empty!"
        });
    }
    
    // Create a Customer
    const url = new URL({
        title: req.body.title,
        link: req.body.url,
    });

    
    // Save Customer in the database
    URL.create(url, (err, data) => {
        if (err)
        res.status(500).send({
            message:
            err.message || "Some error occurred while creating the Customer."
        });
        else res.send(data);
    });
};

// Retrieve all URLs from the database.
exports.findAll = (req, res) => {
    URL.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving urls."
          });
        else res.send(data);
    });
};

// Find a single URL with a urlId
exports.findOne = (req, res) => {
    URL.findById(req.params.urlId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found URL with id ${req.params.urlId}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving URL with id " + req.params.urlId
            });
          }
        } else res.send(data);
    });
};

// Update a URL identified by the urlId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
        message: "Content can not be empty!"
        });
    }

    // Create a Customer
    const url = new URL({
        title: req.body.title,
        link: req.body.url,
    });

    URL.updateById(
        req.params.urlId,
        url,
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found URL with id ${req.params.urlId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating URL with id " + req.params.urlId
                    });
                }
            } else res.send(data);
        }
    );
};

// Delete a URL with the specified urlId in the request
exports.delete = (req, res) => {
    URL.remove(req.params.urlId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                message: `Not found URL with id ${req.params.urlId}.`
                });
            } else {
                res.status(500).send({
                message: "Could not delete URL with id " + req.params.urlId
                });
            }
        } else res.send({ message: `URL was deleted successfully!` });
    }); 
};

// Delete all URLs from the database.
exports.deleteAll = (req, res) => {
    URL.removeAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while removing all URLs."
          });
        else res.send({ message: `All URLs were deleted successfully!` });
    });
};