module.exports = app => {
    const urls = require("../controllers/url.controller.js");
  
    // Create a new url
    app.post("/urls", urls.create);
  
    // Retrieve all urls
    app.get("/urls", urls.findAll);
  
    // Retrieve a single url with urlId
    app.get("/urls/:urlId", urls.findOne);
  
    // Update a url with urlId
    app.put("/urls/:urlId", urls.update);
  
    // Delete a url with urlId
    app.delete("/urls/:urlId", urls.delete);
  
    // Create a new url
    app.delete("/urls", urls.deleteAll);
  };