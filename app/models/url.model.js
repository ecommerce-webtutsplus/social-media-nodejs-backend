const sql = require("./db.js");

// constructor
const URL = function(url) {
  this.title = url.title;
  this.link = url.link;
};

URL.create = (newURL, result) => {
  sql.query("INSERT INTO urls SET ?", newURL, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created url: ", { id: res.insertId, ...newURL });
    result(null, { id: res.insertId, ...newURL });
  });
};

URL.findById = (urlId, result) => {
  sql.query(`SELECT * FROM urls WHERE id = ${urlId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found url: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found URL with the id
    result({ kind: "not_found" }, null);
  });
};

URL.getAll = result => {
  sql.query("SELECT * FROM urls", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("urls: ", res);
    result(null, res);
  });
};

URL.updateById = (id, url, result) => {
  sql.query(
    "UPDATE urls SET title = ?, link = ? WHERE id = ?",
    [url.title, url.link, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found URL with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated url: ", { id: id, ...url });
      result(null, { id: id, ...url });
    }
  );
};

URL.remove = (id, result) => {
  sql.query("DELETE FROM urls WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found URL with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted url with id: ", id);
    result(null, res);
  });
};

URL.removeAll = result => {
  sql.query("DELETE FROM urls", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} urls`);
    result(null, res);
  });
};

module.exports = URL;