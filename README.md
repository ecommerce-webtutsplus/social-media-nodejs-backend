## API Endpoints
* GET /urls - list all URLs
* POST /urls - Create a new url
* GET /urls/:id - Get one url
* PUT /urls/:id - Update one url
* DELETE /urls/:id - Delete one URL
* DELETE /urls - Delete all URLs

## Important things to take care of before Deploying the API
* Update the credentials of the database in config/db.config.js
* Import the test.sql database in mySQL